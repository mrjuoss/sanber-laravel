<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account Baru!</title>
    <link rel="stylesheet" type="text/css"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />

    <style>
        body {
            margin: 50px;
        }
    </style>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ url('/welcome') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="first_name">First name:</label>
            <br />
            <input type="text" name="first_name" id="first_name" class="form-control" value={{ old('first_name') }}>
        </div>
        <div class="form-group">
            <label for="last_name">Last name:</label>
            <br />
            <input type="text" name="last_name" id="last_name" class="form-control" value={{ old('last_name') }}>
        </div>
        <div>
            <p>Gender:</p>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="male" value="Male"
                    {{ (old('gender') == 'Male') ? 'checked' : ''}}>
                <label for="male">Male</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="female" value="Female"
                    {{ (old('gender') == 'Female') ? 'checked' : ''}}>
                <label for="female">Female</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="other_gender" value="Other"
                    {{ (old('gender') == 'Other') ? 'checked' : ''}}>
                <label for="other_gender">Other</label>
            </div>
        </div>
        <div class="form-group">
            <label for="nationality-select">Nationality:</label>
            <br />
            <select name="nationality" id="nationality-select" class="form-control">
                <option value="WNI" {{ old('nationality') == "WNI" ? 'selected' : '' }}>Indonesian</option>
                <option value="WNA" {{ old('nationality') == "WNA" ? 'selected' : '' }}>Warga Negara Asing</option>
            </select>
        </div>
        <div>
            <p>Language Spoken:</p>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="languages[]" id="bahasa_indonesia"
                    value="Bahasa Indonesia"
                    {{ (is_array(old('languages')) and in_array('Bahasa Indonesia', old('languages'))) ? ' checked' : '' }}>
                <label for="bahasa_indonesia">Bahasa Indonesia</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="languages[]" id="bahasa_inggris"
                    value="English Language"
                    {{ (is_array(old('languages')) and in_array('English Language', old('languages'))) ? ' checked' : '' }}>
                <label for="bahasa_inggris">English</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="languages[]" id="other_language" value="Other"
                    {{ (is_array(old('languages')) and in_array('Other', old('languages'))) ? ' checked' : '' }}>
                <label for="other_language">Other</label>
            </div>
        </div>
        <div class="form-group">
            <label for="bio">Bio:</label>
            <br />
            <textarea name="bio" id="bio" rows="5" cols="33" class="form-control">{{old('bio')}}</textarea>
        </div>
        <div>
            <input type="submit" class="btn btn-primary" value="Sign Up" />
        </div>
    </form>
</body>

</html>
