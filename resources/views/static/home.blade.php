<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Selamat Datang</title>
    <link rel="stylesheet" type="text/css"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <style>
        body {
            margin: 50px;
        }
    </style>
</head>

<body>
    <div>
        <h1>SELAMAT DATANG! {{$dataValid['first_name'] }} {{$dataValid['last_name']}}</h1>
    </div>
    <div>
        <p>
            <b>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</b>
        </p>
        <h2>Data yang telah anda input adalah sebagai berikut : </h2>
        <ul>
            <li>Nama Depan : {{ $dataValid['first_name'] }}</li>
            <li>Nama Belakang : {{ $dataValid['last_name'] }}</li>
            <li>Jenis Kelamin : {{ $dataValid['gender'] }}</li>
            <li>Status Kewarganegaraan : {{ $dataValid['nationality'] }}</li>
            <li>Bahasa yang dikuasai :
                <ul>
                    @foreach($dataValid['languages'] as $language)
                    <li>{{ $language }}</li>
                    @endforeach
                </ul>
            </li>
            <li>Tentang Anda : {{ $dataValid['bio'] }}</li>
        </ul>
    </div>
</body>
