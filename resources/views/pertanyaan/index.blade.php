@extends('layout.master')

@push('styles')
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}} ">
@endpush

@section('page_info')
<a href="{{ url('/pertanyaan/create')}}" class="px-4 mt-2 btn btn-primary btn-sm">Tambah Data</a>
@endsection
@section('page_active', 'Pertanyaan')
@section('action', 'Create')

@section('content')
<section class="content">
    <div class="mx-2 card">
        <div class="card-header bg-primary">
            <h3 class="card-title">Daftar Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

            @if(session('success'))
            <div class="alert alert-primary" role="alert">
                {{ session('success') }}
            </div>
            @endif

            <table id="pertanyaan_table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Judul</th>
                        <th class="text-center">Isi Pertanyaan</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->judul }}</td>
                        <td>{{ $item->isi }}</td>
                        <td>
                            <a href="/pertanyaan/{{ $item->id }}" class="text-white w-100 btn btn-primary btn-xs">
                                Show
                            </a>
                            <a href="/pertanyaan/{{ $item->id }}/edit" class="w-100 btn btn-warning btn-xs">
                                Edit
                            </a>
                            <form action="/pertanyaan/{{ $item->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="text-white w-100 btn btn-danger btn-xs" value="Delete">

                            </form>

                        </td>

                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Judul</th>
                        <th class="text-center">Isi Pertanyaan</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
</section>
@endsection

@push('scripts')

<script src="{{ asset('admin_lte/plugins/datatables/jquery.dataTables.js')}} "></script>
<script src="{{ asset('admin_lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}} "></script>
<script>
    $(function () {
    $("#pertanyaan_table").DataTable();
  });
</script>

@endpush
