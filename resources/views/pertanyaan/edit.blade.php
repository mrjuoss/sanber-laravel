@extends('layout.master')

@section('page_active', 'Pertanyaan')
@section('action', 'Edit')

@section('content')
<!-- /.container-fluid -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Pertanyaan</h3>
                    </div>
                    <!-- /.card-header -->

                    <!-- form start -->
                    <form role="form" name="form_pertanyaan" action="/pertanyaan/{{ $data->id }}" method="POST">
                        <!-- /.card-header -->
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label for="judul">Judul Pertanyaan</label>
                                        <input type="text" id="judul" class="form-control @error('judul') is-invalid
                                        @enderror" name="judul" value="{{ old('judul', $data->judul),  }}"
                                            placeholder="Ketikkan judul pertanyaan">
                                        @error('judul')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="isi">Isi Pertanyaan</label>
                                        <textarea id="isi" class="form-control @error('isi') is-invalid
                                        @enderror" name="isi" rows="3"
                                            placeholder="Ketikkan isi pertanyaan">{{ old('isi', $data->isi) }}</textarea>
                                        @error('isi')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
