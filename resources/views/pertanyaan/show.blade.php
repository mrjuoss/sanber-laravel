@extends('layout.master')

@section('page_active', 'Pertanyaan')
@section('action', 'Show')

@section('content')
<!-- /.container-fluid -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Pertanyaan</h3>
                    </div>
                    <!-- /.card-header -->

                    <!-- form start -->
                    <form role="form" name="form_pertanyaan" action="/pertanyaan" method="POST">
                        <!-- /.card-header -->
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label for="judul">Judul Pertanyaan</label>
                                        <input type="text" id="judul" class="form-control name=" judul"
                                            value="{{ $data->judul }}" placeholder="Ketikkan judul pertanyaan" disabled>

                                    </div>
                                    <div class="form-group">
                                        <label for="isi">Isi Pertanyaan</label>
                                        <textarea id="isi" class="form-control name=" isi" rows="3"
                                            placeholder="Ketikkan isi pertanyaan" disabled>{{ $data->isi }}</textarea>

                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="card-footer">
                    <a href="/pertanyaan" class="btn btn-primary">Back</a>
                </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
