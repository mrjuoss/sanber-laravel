<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('templating.table');
});

Route::get('/data-tables', function () {
    return view('templating.data_table');
});

Route::get('/static', function () {
    return view('static.index');
});

Route::get('/home', "HomeController@index");

Route::get('/register', "AuthController@register");

Route::post('/welcome', "AuthController@store");

Route::get('/master', function() {
    return view("layout.master");
});

Route::get('/blank', function() {
    return view("blank");
});

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');
