<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{

    public function index()
    {
        $data = DB::table("pertanyaan")->get();
        return view ("pertanyaan.index", compact("data"));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|string|max:45|unique:pertanyaan',
            'isi' => 'required'
        ]);

        DB::table('pertanyaan')->insert([
            'judul' => $request->judul,
            'isi' => $request->isi,
        ]);

        return redirect("/pertanyaan")->with('success', 'Berhasil menambah data');
    }

    public function show($id)
    {
        $data = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.show', compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|string|max:45',
            'isi' => 'required'
        ]);

        DB::table('pertanyaan')
                ->where('id', $id)
                ->update([
                    'judul' => $request->judul,
                    'isi' => $request->isi,
                ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil melakukan updating data');
    }

    public function destroy(Request $request, $id)
    {
        DB::table('pertanyaan')->where('id', $id)->delete();

        return redirect('/pertanyaan')->with('success','Berhasil menghapus data');
    }
}
