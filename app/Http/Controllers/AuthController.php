<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function index()
    {
        return view("static.index");
    }

    public function register()
    {
       return view("static.register");
    }

    public function store(Request $request)
    {
        $request->validate([
           'first_name' => 'required|string|max:50',
           'last_name' => 'required|string|max:50',
           'gender' => 'required',
           'nationality' => 'required',
           'languages' => 'required',
           'bio' => 'required'
        ]);

        $dataValid = $request->all();

        return view("static.home", compact("dataValid"));
    }


}
